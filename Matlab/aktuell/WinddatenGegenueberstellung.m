%% Tradewind acting on central pacific - Anomaly Datas -
C_tradewind=importdata('trade_center_anomaly.txt');
daten=C_tradewind.data(1:41,2:end);   % fit fuer die Jahre 1979 - 2019
[N,~]=size(daten);
tw_center=daten(1,:);

for i=2:N
    tw_center=[tw_center, daten(i,:)];
end
%% North_East tradewind acting on west pacific - Anomaly Datas -
NW_tradewind=importdata('trade_west_anomaly.txt');
daten=NW_tradewind.data(1:41,2:end);   
[N,~]=size(daten);
tw_west=daten(1,:);

for i=2:N
    tw_west=[tw_west, daten(i,:)];
end

%% SouthEast tradewind acting on east pacific - Anomaly Datas -
SE_tradewind=importdata('trade_east_anomaly.txt');
daten=SE_tradewind.data(1:41,2:end);
[N,~]=size(daten);

tw_east=daten(1,:);

for i=2:N
    tw_east=[tw_east, daten(i,:)];
end


tw_center=smoothdata(tw_center,'gaussian',5);
tw_west=smoothdata(tw_west,'gaussian',5);
tw_east=smoothdata(tw_east,'gaussian',5);

startdate = datenum('01-1979','mm-yyyy');
enddate = datenum('12-2019','mm-yyyy');
time = linspace(startdate,enddate,492);

figure(1)
plot(time, tw_center,  time, tw_west, time, tw_east, 'LineWidth',1.5);
refline(0,0);
grid;
datetick('x','yyyy','keepticks')
xlabel('Zeit t in [month]','Fontsize',12);
ylabel('Anomalien in [mbar]','Fontsize',12);
legend('Zentrum(175�W-140�W)','Westen(135�E-180�W)','Osten(135�W-120�W)','Fontsize',10);
title('Gegenueberstellung Winddaten West-, Zentral- und Ostpazifik','Fontsize',13);
