function [h_w , h_e] = thermocline_fit()
% Rueckgabewert Thermoclinen-Anomalie [h_west, h_ost] fuer die Jahre 
% Jan 1980 - Oct 2008

%%  west pacific - gemessene Thermoklinenanomalie -
thermocline=importdata('hw_anomaly.txt');   % Daten zu lesen: Zeilenweise
[N,~]=size(thermocline);
h_w=thermocline(1,:);

for i=2:N
    h_w=[h_w, thermocline(i,:)];
end
h_w=h_w(1: end-4);              % in der letzten Zeile ist nur ein Eintrag, die anderen 4 Stellen sind leer!

%% east pacific - gemessene Thermoklineanomalie -
thermocline=importdata('he_anomaly.txt');
[N,~]=size(thermocline);
h_e=thermocline(1,:);

for i=2:N
    h_e=[h_e, thermocline(i,:)];
end

h_e=h_e(1:end-4);           % Abschneiden der leeren Eintr�ge (siehe h_w)

end