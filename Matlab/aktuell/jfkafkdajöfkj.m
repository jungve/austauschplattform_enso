function [curve, fouRei] = externtradewindEast()
%EXTERNTRADEWIND 
% Funktion extrahiert den netto-Passatwindanteil aus dem gemessenen Wind
% ueber dem Ostpazifik. Hierzu wird der gemessene Gesamt-Wind in eine
% Hoehenanomalie umgerechnet (mittels des bestimmten Parameters tetha_ost) und
% mit dem Anteil aus SST verrechnet.
% --> externer Systeminput: netto Passatwinde

%% theta und verschiebung von extern
% [theta_w, theta_e, v_w, v_e] = parameter_theta();
theta=-4.8941; 
%theta = -9;

%% SouthEast tradewind acting on east pacific - Anomaly Datas -

SE_tradewind=importdata('trade_east_anomaly.txt');
daten=SE_tradewind.data(:,2:end);   % verfuegbar fuer die Jahre 1979 - 2019
[N,~]=size(daten);
tw_east=daten(1,:);

for i=2:N
    tw_east=[tw_east, daten(i,:)];
end                         

tw_east=smoothdata(tw_east,'gaussian',10);  % Glaettung




%% Bestimmung des Einflusses des passatwindes ohne SST Anteil
SST=importdata('SST.txt');
SST_E=SST.data(349:840,6);      % Jan 1979 - 2019, passend zu verfuegbaren Winddaten; 6.Spalte fuer Ost Anomalie
SST_E=smoothdata(SST_E,'gaussian',10);

%epsilon2=1/2;
%tau=epsilon2*b*SST_E;           % tau: Einflussanteil an der Thermoklinenhoehenaenderung erzeugt durch die SST
gamma=0.077;
tau=1/gamma*(SST_E);                     % VZ positiv. SST negativ --> mehr wind --> hoehenanomalie negativ

he_func=theta*tw_east;            % theta negativ --> staerkerer gemessener wind --> hoehenanomalie negativ
netto_tradewind=(he_func-tau');    % he_func und tau beide richtige VZ --> um netto tw mit richtigem VZ zu bekommen abziehen?  

%%%%%%%%%%%%%% "Funktionen"fit aus den verrechneten und extrahierten
%%%%%%%%%%%%%% Messwerten
time=1:length(SST_E);
curve = fit(time',netto_tradewind','smoothingspline');
startdate = datenum('01-1979','mm-yyyy');
enddate = datenum('12-2019','mm-yyyy');
dt = linspace(startdate,enddate,492);

%%%%%%%%%%%%%% moegliche Prognose mittels Fourierreihe %%%%%%%%%%%%%%
 fouRei = fit(time',netto_tradewind','fourier8');
% % coeffs = coeffvalues(fouRei);
% % coeffs(:,18)=0.1047;  % Entspricht 5-Jahreszyklus (hat am besten gepasst)
% % f3 = fit(time',netto_tradewind','fourier8', 'StartPoint', coeffs);
% % 
% % fou= @(t) fouRei(t)+f3(t);    %�berlagerung
% 
% startdate = datenum('01-1979','mm-yyyy');
% enddate = datenum('10-2044','mm-yyyy');
% dt2 = linspace(startdate,enddate,800);
% zeit=1:800;

%%%%%%%%%%%%%% Visualisierung Windzusammensetzung %%%%%%%%%%%%%%
% figure(12)
% plot(dt, he_func, dt, netto_tradewind, dt, tau,'LineWidth',1.5);
% refline(0,0);
% datetick('x','yyyy','keepticks')
% xlabel('Zeit t in [month]','Fontsize',12);
% ylabel('Anomalien in [m]','Fontsize',12);
% legend('h_e gesamt, berechnet','netto SE-Passatwind','SST Anteil','Fontsize',10);
% title('Windzusammensetzung Osten','Fontsize',13);

%%%%%%%%%%%%%% moegliche Prognose mittels Fourierreihe %%%%%%%%%%%%%%
% figure(199)
%  plot(dt,netto_tradewind, dt2,fouRei(zeit),'LineWidth',1.5)
%  datetick('x','yyyy','keepticks')
%  legend('orginal', 'FourierReihe','Fontsize',10)
%  title('Fourierreihe f�r Wind-Prognose Ost','Fontsize',13)
%  xlabel('Zeit t in [month]','Fontsize',12);
%  ylabel('Anomalien in [m]','Fontsize',12);
%  xlim([startdate,enddate]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

end