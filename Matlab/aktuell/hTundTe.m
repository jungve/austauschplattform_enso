function hTundTe(t,he,hw,Te)
 l=-50:0.01:50;

 [tww, fourierWest]=externtradewindWest();
[twe, fourierEast]=externtradewindEast();

 % An Durchschnittshöhe 20° Thermokline anpassen
 hw = -hw - 150;
 he = -he - 100;
 
 normTK=interp1([50 -50],[-100 -150],l, 'pchip');
 
 c = jet(32);       %Farbcode Temperatur
 
% Prepare the new file. 
vidObj=VideoWriter('Thermokline.avi');
open(vidObj);

h = figure(1);
set(h,'Position',[200 200 800 420]);

for j = 1:length(t)
    % Aktualisiere Mittlere Thermokline
%     h= 1/2 * (he(j)+hw(j));
%     tk=ones([1,length(l)]).*(h);
    
    % Aktualisiere Thermokline
    ip=interp1([50 -50],[he(j) hw(j)],l, 'pchip');
    
    % Aktualisier Temperatur-Farbe
    idx = round(interp1([min(Te) max(Te)], [1 32], Te(j)));
    
    %Aktualisiere Pfeillänge
%     B_w=[-1-tww(j),5];
%     d=A_W-B_W;
    
    
    % Plot Thermokline
    plot(l,ip,'r',l,normTK,'b')
   
    hold on

    % Plot Temperatur 
    plot(45,-25,'o',...
    'Markersize',40,...
    'MarkerFaceColor',c(idx,:))
    
    % Plot Wind 
    quiver(-35,10,-tww(j)/2.5,0,0,'Color','b','LineWidth', 10);%,'AutoScale' ,'off','AutoScaleFactor',0.4,'MaxHeadSize',90)
    quiver(35,10,-twe(j)/2.5,0,0,'Color','b','MaxHeadSize',0.9,'LineWidth', 10);%,'AutoScale' ,'off','AutoScaleFactor',0.4)
%         quiver(-35,15,-tww(j)/2.5,0,0,'Color','b','LineWidth', 1);
%     quiver(35,15,-twe(j)/2.5,0,0,'Color','b','MaxHeadSize',0.9,'LineWidth', 1)
%         quiver(-35,12,-tww(j)/2.5,0,0,'Color','b','LineWidth', 1);
%     quiver(35,12,-twe(j)/2.5,0,0,'Color','b','MaxHeadSize',0.9,'LineWidth', 1)
%         quiver(-35,17,-tww(j)/2.5,0,0,'Color','b','LineWidth', 1);
%     quiver(35,17,-twe(j)/2.5,0,0,'Color','b','MaxHeadSize',0.9,'LineWidth', 1)
%         quiver(-35,20,-tww(j)/2.5,0,0,'Color','b','LineWidth', 1);
%     quiver(35,20,-twe(j)/2.5,0,0,'Color','b','MaxHeadSize',0.9,'LineWidth', 1)
%         quiver(-35,22,-tww(j)/2.5,0,0,'Color','b','LineWidth', 1);
%     quiver(35,22,-twe(j)/2.5,0,0,'Color','b','MaxHeadSize',0.9,'LineWidth', 1)
    
    axis([-50 50 -200 100])    
    grid;
    refline(0,0);
    xlabel('Längengrade');
    ylabel('h');
%     [~,icons,~,~] = legend('Thermokline','mittlere Thermokline', 'Temperatur');
%     icons(9).MarkerSize=10;

    hold off
%     colorbar;
% Write each frame to the file.
currFrame = getframe();
writeVideo(vidObj,currFrame);
end

 % Close the file.
    close(vidObj);

 
  
% clear all
end


