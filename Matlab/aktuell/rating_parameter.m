%%%%%%%%%%%%%%%% Parameter Setting %%%%%%%%%%%%%%%%
r=1/6.25;
alpha=0.67;
eps1=1/2.75;
eps2=1/2;
gamma=0.077;
b=14;
%%
%%%%%%%%%%%%%%%% Orginaldaten Import zum Vergleich %%%%%%%%%%%%%%%%
[h_w , h_e] = thermocline_fit();
SST = importdata('SST.txt');
SST_E=SST.data(350:841,6);
soi=SOI_fit();

% Daten-Gl�tten
h_w=smoothdata(h_w,'gaussian',10);
h_e=smoothdata(h_e,'gaussian',10);
SST_E=smoothdata(SST_E,'gaussian',10);
soi=smoothdata(soi,'gaussian',10);

% Externe Anretungen Westen und Osten
[curvewest, fourierWest]=externtradewindWest();
[curveeast, fourierEast]=externtradewindEast();

% Koeffizienten Osten
o=0.6:0.1:1.7;
% Koeffizienten Westen
w=0.6:0.1:1.7;

%%%%%%%%%%%%%%%% Definition System %%%%%%%%%%%%%%%% (hhT == (h_W, h_E, T_E) in ([m], [m], [K])
for i=1:length(w)
    for j=1:length(o)

d_dt = @(t,hhT)   [ -r * hhT(1)                         - r* alpha*b*hhT(3)     - r *w(i)* curvewest(t)      ;      % Anregung Nordpassat 
                    eps2*hhT(1) -   eps2*hhT(2)         + eps2*b*hhT(3)         - eps2 *o(j)* curveeast(t)   ;      % Anregung Suedpassat VZ umgedreht, weil der passat in der Windzusammensetzung falsches vz hat
                    %1.2 ost 0.8 west
                                      eps1*gamma*hhT(2)   - eps1*hhT(3)                                   ];

%%%%%%%%%%%%%%%% Systemverhalten (Jan 79 - Dez 2019)  %%%%%%%%%%%%%%%%
time=1:1:492;      % [month]   

[t, hhT] = ode45(d_dt, time , [-2.14 5.82 0.38] );

% Korrelation Thermokline Westen
% Fehlerberechung
e_w=0;
    for k=1:length(h_w)
        if isnan(h_w(k))|isnan(hhT(12+k,1))
            e_w(k)=0;
        else
            e_w(k)=norm(h_w(k)-hhT(12+k,1));
        end
    end 
m_w(i,j)=mean(e_w);       % Mittelwert          


% Korrelation Thermokline Osten
e_e=0;
 for k=1:length(h_w)
        if isnan(h_w(k))|isnan(hhT(12+k,2))
            e_e(k)=0;
        else
            e_e(k)=norm(h_w(k)-hhT(12+k,2));
        end
    end 
m_e(i,j)=mean(e_e);       % Mittelwert 


% Korrelation Temperatur
 for k=1:length(SST_E)
        if isnan(SST_E(k))|isnan(hhT(k,3))
            e_t(k)=0;
        else
            e_t(k)=norm(SST_E(k)-hhT(k,3));
        end
 end
m_t(i,j)=mean(e_t);       % Mittelwert

% Korrelation SOI
soi_h=soi_calc(hhT(:,1),hhT(:,2));
R = corrcoef(soi,soi_h);
Rsoi(i,j) = R(2,1);   
    end
end

[c_w, ind_w]=min(m_w);
[c_ww, ind_ww]=min(c_w);
wmax1=w(ind_w(ind_ww));
omax1=o(ind_ww);

[c_e, ind_e]=min(m_e);
[c_ee, ind_ee]=min(c_e);
wmax2=w(ind_e(ind_ee));
omax2=o(ind_ee);

[c_t, ind_t]=min(m_t);
[c_tt, ind_tt]=min(c_t);
wmax3=w(ind_t(ind_tt));
omax3=o(ind_tt);

[c_s, ind_s]=max(Rsoi);
[c_ss, ind_ss]=max(c_s);
wmax4=w(ind_s(ind_ss));
omax4=o(ind_ss);