function [p_west, p_east] = pressure_fit()
%%% sea-level-pressure for SOI 
% Laden der gemessenen Druck-Daten
% Daten von Jan 1979 bis Dez 2019

%% sea-level-pressure at Indonesia - Standartisized Datas -
SL_Pressure_West=importdata('pressure_west.txt');
daten=SL_Pressure_West(31:end,2:end);   % Daten erst ab 1979 relevant
[N,~]=size(daten);
p_west=daten(1,:);

for i=2:N
    p_west=[p_west, daten(i,:)];
end

% p_west=p_west(1:346);    % Jan 1980 - Oct 2008 (346 Monate)

%% sea-level-pressure in Eastern Pacific - Standartisized Datas -
SL_Pressure_East=importdata('pressure_east.txt');
daten=SL_Pressure_East(31:end,2:end);  % Daten erst ab 1979 relevant
[N,~]=size(daten);

p_east=daten(1,:);

for i=2:N
    p_east=[p_east, daten(i,:)];
end

% p_east=p_east(1:346);    % Jan 1980 - Oct 2008 (346 Monate)

end