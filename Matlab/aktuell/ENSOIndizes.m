%% EQ Southern Oscilation Index
EQSOI_Data=importdata('SOI_Equatorial.txt');
daten=EQSOI_Data(3:end,2:end);   % alle ab 51 verfuegbar
[N,~]=size(daten);
eqsoi=daten(1,:);

for i=2:N-1
    eqsoi=[eqsoi, daten(i,:)];
end
eqsoi=[eqsoi, daten(N,1:4)];
eqsoi=smoothdata(eqsoi,'gaussian',10);

%% ONI
ONI_Data=importdata('ONI.txt');
oni=ONI_Data(13:end,5);   % alle ab 51 verfuegbar
% [N,~]=size(daten);
% eqsoi=daten(1,:);
% 
% for i=2:N
%     eqsoi=[eqsoi, daten(i,:)];
% end

oni=smoothdata(oni,'gaussian',10);

%% SOI
SOI_Data=importdata('SOI.txt');
daten=SOI_Data(1:end,2:end);   % alle ab 51 verfuegbar
[N,~]=size(daten);
soi=daten(1,:);

for i=2:N-1
    soi=[soi, daten(i,:)];
end
soi=[soi, daten(N,1:4)];
soi=smoothdata(soi,'gaussian',10);


%%%%%%%%%%%%%%%% Nur SOI %%%%%%%%%%%%%%%%%
pos = soi;
 pos(pos<0)=nan;
 neg = soi;
 neg(neg>0)=nan; 

startdate = datenum('01-1951','mm-yyyy');   % Langer Verlauf
enddate = datenum('04-2020','mm-yyyy');
t = linspace(startdate,enddate,832);
 
 figure(2)
 area(t, pos,'FaceColor','b' ,'EdgeColor','k')
 title('\fontsize{35}Southern Oscillation Index - SOI')
 datetick('x','yyyy','keepticks')
 xlim([startdate, enddate]);
  set(gca, 'FontSize',30);
 ylim([-3, 2.6]);
 set(gca, 'FontSize',30);
 hold on
 area(t, neg,'FaceColor','r' ,'EdgeColor','k')
  legend('\fontsize{30}La Nina', '\fontsize{30}El Nino')
hold off 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% alle 3 Indizes %%%%%%%%%%%%%
% % Fuer Plot mit angezeigten Jahren
startdate = datenum('01-1979','mm-yyyy');    % angepasst an Daten Simulation
enddate = datenum('04-2020','mm-yyyy');
t = linspace(startdate,enddate,496);

 pos = soi(337:end);
 pos(pos<0)=nan;
 neg = soi(337:end);
 neg(neg>0)=nan;
 
 
 pos2 = eqsoi(337:end);
 pos2(pos2<0)=nan;
 neg2 = eqsoi(337:end);
 neg2(neg2>0)=nan;
 
  pos3 = oni(337:end);
 pos3(pos3<0)=nan;
 neg3 = oni(337:end);
 neg3(neg3>0)=nan;
 
 figure(1)
 subplot(3,1,1)
 area(t, pos,'FaceColor','b' ,'EdgeColor','k')
 title(['\fontsize{15}Southern Oscillation Index - SOI'])
 datetick('x','yyyy','keepticks')
 xlim([startdate, enddate]);
set(gca, 'FontSize',12);
 ylim([-2.6, 2.6]);
 set(gca, 'FontSize',12);
 hold on
 area(t, neg,'FaceColor','r' ,'EdgeColor','k')
  legend(['\fontsize{10}La Nina'], ['\fontsize{10}El Nino'])
hold off 
subplot(3,1,2)
 area(t, pos2,'FaceColor','b' ,'EdgeColor','k')
 title(['\fontsize{15}Equatorial Southern Oscillation Index - EQSOI'])
 hold on
 datetick('x','yyyy','keepticks')
 xlim([startdate, enddate]);
 set(gca, 'FontSize',12);
 ylim([-2.6, 2.6]);
 set(gca, 'FontSize',12);
 area(t, neg2,'FaceColor','r' ,'EdgeColor','k') 
legend(['\fontsize{10}La Nina'], ['\fontsize{10}El Nino'])
 
hold off
subplot(3,1,3)
 area(t, pos3,'FaceColor','r','EdgeColor','k')
 title(['\fontsize{15}Oceanic Nino Index - ONI'])
 hold on
 datetick('x','yyyy','keepticks')
 xlim([startdate, enddate]);
 set(gca, 'FontSize',12);
 ylim([-2.6, 2.6]);
 set(gca, 'FontSize',12);
 area(t, neg3,'FaceColor','b','EdgeColor','k')
  legend({['\fontsize{10}El Nino'], ['\fontsize{10}La Nina']},'Location','Southeast')
hold off
 

