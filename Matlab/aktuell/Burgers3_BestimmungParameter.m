%%%%%%%%%%%%%%%% Daten laden %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%% Orginaldaten Import zum Vergleich %%%%%%%%%%%%%%%%
[h_w , h_e] = thermocline_fit();
SST = importdata('SST.txt');
SST_E=SST.data(350:841,6);
soi=SOI_fit();
% Daten-Gl�tten
h_w=smoothdata(h_w,'gaussian',10);
h_e=smoothdata(h_e,'gaussian',10);
SST_E=smoothdata(SST_E,'gaussian',10);
soi=smoothdata(soi,'gaussian',10);
%%%%%%%%%%%%%%%% Externe Anretungen Westen und Osten %%%%%%%%%%%%%%%%
[curvewest, fourierWest]=externtradewindWest();
[curveeast, fourierEast]=externtradewindEast();

%%%%%%%%%%%%%%%% Parameter Setting %%%%%%%%%%%%%%%%
r=1/6.25;
alpha=0.67;
eps1=1/2.75;
eps2=1/2;
gamma=0.077;
b=14;
%%%%%%%%%%%%%%%% Zu untersuchende Parameter %%%%%%%%%%%%%%%%
beta1=0.1:0.1:1.5;   % Vorfaktor Nordpassat
beta2=0.1:0.1:1;   % Vorfaktor S�dpassat

%"startwerte" f�r die mittlere Abweichung
% sst=250;
% hw=250;
% he=250;
% bew=(sst+he+hw)/3;
% beta_2=beta2(1);

%"startwert" f�r Korrelation
cor_f=0.1;

time=1:1:492;      % [month] 

for i=1:length(beta1)
    beta_1=beta1(i);
for j=1:length(beta2)
    beta_2=beta2(j);
%%%%%%%%%%%%%%%% Definition System %%%%%%%%%%%%%%%% (hhT == (h_W, h_E, T_E) in ([m], [m], [K])
d_dt = @(t,hhT)   [ -r * hhT(1)                         - r* alpha*b*hhT(3)     - r *beta_1* curvewest(t)      ;      % Anregung Nordpassat 
                    eps2*hhT(1) -   eps2*hhT(2)         + eps2*b*hhT(3)         - eps2 *beta_2* curveeast(t)   ;      % Anregung Suedpassat VZ umgedreht, weil der passat in der Windzusammensetzung falsches vz hat
                                    eps1*gamma*hhT(2)   - eps1*hhT(3)                                   ];
                                                      
%%%%%%%%%%%%%%%% Systemverhalten (Jan 79 - Dez 2019)  %%%%%%%%%%%%%%%%
  
[t, hhT] = ode45(d_dt, time , [-2.14 5.82 0.38] );

%%%%%% mit dem Mittelwert der mittleren Fehler kommt fuer die Vorfaktoren
%%%%%% 0.5 und 0.1 oder so ... --> voelliger mist
% d_hw=mean(abs(hhT(1:346,1)-h_w'));
% d_he=mean(abs(hhT(1:346,2)-h_e'));
% d_sst=mean(abs(hhT(:,3)-SST_E));
% d_bew=(d_sst+d_he+d_hw)/3;
% 
% if (d_bew < bew)
%     bew=d_bew;
%     sst=d_sst;
%     hw=d_hw;
%     he=d_he;
%     beta_ij=[i j];
% end

% %%%%%%%%%%%%%%%%  Vergleich mit ENSO index ios equatorial   %%%%%%%%%%%%%%%%

%%%%%%%% Hiermit kommt fuer die Vorfaktoren 0.2 und 0.2 raus. -->
%%%%%%%% Vielleicht fallen die Jahre 2007 bis 2014 mit dem Gegensaetzlichen Ausschlaegen, so schwer ins Gewicht,
%%%%%%%% dass es besser ist gar keine Ausschlaege zu haben?! --> Vielleicht
%%%%%%%% einfach gar keine Parameter bzw. einfach bei 1 lassen und so tun als haetten wir es nie angesprochen....
%%%%%%%% ... bin deprimiert.

% %Korrelations-Koeffizient: Wie stark stimmen die beiden Kurven �berein?
% %Maximale �bereinstimmung f�r R=1;
soi_h=soi_calc(hhT(:,1),hhT(:,2));
R = corrcoef(soi,soi_h);
R = R(2,1);
if (R > cor_f)
    cor_f=R;
    beta_ij=[i j];
end


end
end

erg_beta1=beta1(beta_ij(1));
erg_beta2=beta2(beta_ij(2));

%%



%%
% % %%%%%%%%%%%%%%%% Visualisierung %%%%%%%%%%%%%%%%
% % Fuer Plot mit angezeigten Jahren
% startdate = datenum('01-1979','mm-yyyy');
% enddate = datenum('12-2019','mm-yyyy');
% dt = linspace(startdate,enddate,492);
% % 
% %%% Simulationsergebnisse Modell
% figure(1)
% plot(dt, hhT(:,1), dt, hhT(:,2) , dt, hhT(:,3), 'LineWidth', 1.5)
% grid;
% datetick('x','yyyy','keepticks')
% title('Nachbildung ENSO - Burgers(3)- Passatwinde 1979 - 2019', 'Fontsize', 13)
% legend('h_W in [m]', 'h_E in [m]', 'Temperatur in [K]', 'Fontsize', 10);
% xlabel('Zeit t in [month]', 'Fontsize', 12)
% ylabel('Anomalien', 'Fontsize', 12)
% 
% 
% %%% Vergleichsplots Simulation und Orginaldaten
% t=dt(13:358);                  
% figure(2)
% plot(t, hhT(13:358,1), t, h_w, 'LineWidth', 1.5)
% datetick('x','yyyy','keepticks')
% grid;
% title('h_W-Vergleich: Burgers(3) mit ext. Passatwinde 1980 - 2008', 'Fontsize', 13)
% legend('h_W simuliert', 'h_W gemessen', 'Fontsize', 10);
% xlabel('Zeit t in [month]', 'Fontsize', 12)
% ylabel('Hoehen-Anomalie in [m]', 'Fontsize', 12)
% 
% figure(3)
% plot(t, hhT(13:358,2), t,  h_e, 'LineWidth', 1.5)
% datetick('x','yyyy','keepticks')
% grid;
% title('h_E-Vergleich: Burgers(3) mit ext. Passatwinde 1980 - 2008', 'Fontsize', 13)
% legend('h_E simuliert','h_E gemessen', 'Fontsize', 10);
% xlabel('Zeit t in [month]', 'Fontsize', 12)
% ylabel('H�hen-Anomalie in [m]', 'Fontsize', 12)
% 
% figure(4)
% plot(dt, hhT(:,3), dt,  SST_E, 'LineWidth', 1.5)
% datetick('x','yyyy','keepticks')
% grid;
% title('SST-Vergleich: Burgers(3) mit ext. Passatwinde 1980 - 2008', 'Fontsize', 13)
% legend('Temperatur simuliert','Temperatur gemessen', 'Fontsize', 10);
% xlabel('Zeit t in [month]', 'Fontsize', 12)
% ylabel('Temperatur-Anomalie in [K]', 'Fontsize', 12)
% 
% %%% Vergleich SOI
% figure(100)
% plot(dt,soi, dt, hhT(:,3),dt, soi_h, 'LineWidth', 1.5)
% refline(0,0)
% grid;
% datetick('x','yyyy','keepticks')
% title('Vergleich mit SOI 1979 - 2019', 'Fontsize', 13)
% legend('SOI','Temperatur in [K]','SOI aus Thermoklinenh�hen', 'Fontsize', 10);
% xlabel('Zeit t in [month]', 'Fontsize', 12)
% ylabel('Index/Anomalie', 'Fontsize', 12)
