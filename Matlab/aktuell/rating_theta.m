function [theta,m,s] = rating_theta(h_a, TradeWind, v)
%RATING_THETA 
% Funktion berechnet für verschiedene Verschiebungen v (h_w = tw_w + v)einen
% geeigneten Faktor Theta, um den Passatwind in eine Thermoklinenhöhen-
% änderung umzurechnen (h_w(angenaehert)=tw_w * theta).  Anschließend
% werden alle theta-Werte bewertet, indem der Mittelwert und die
% Standartabweichung des Fehlers bei derAnnaeherung von h_w durch theta*tw_w
% berechnet wird. 
% Die Funktion gibt alle theta-Werte und die dazugehörigen Mittelwerte und
% Standatabweichungen zurück

for i=1:length(v)
    %Verschiebung v
    if v(i)>=0
        h=h_a(1+v(i):end);               % Verschiebung (tradewinds vor h_w auf Grund von Reaktionszeit)
        tw=TradeWind(1:end-v(i));
    else
        h=h_a(1:end+v(i));                            
        tw=TradeWind(1-v(i):end);
    end

    % Bestimmung eines Linearen Zusammenhangs zwischen Wind und Thermokline
    fc = 'a(1) * wind';   
    f  = inline(fc,'a','wind');
    as = 5;
    % Bestimmung Umrechnungsfaktor h_w_ges[m]= theta[m/mbar]*tradewinds[mbar]
    theta(i) = nlinfit(tw,h,f,as);

    % Bewertung theta mittels Mittelwert und Standartabweichung des Fehlers
    % bei der Berechnung von h_w mit  jeweiligem theta
    t=1:length(h);
    hn=theta(i).*tw;            % Thermoklinen-Tiefe berechnet aus theta
   
    % Fehlerberechung
    for j=1:length(h)
        if isnan(h(j))|isnan(hn(j))
            e(j)=0;
        else
            e(j)=norm(h(j)-hn(j));
        end
    end 
    m(i)=mean(e);       % Mittelwert          
    s(i)=std(e);        % Standartabweichung
end 
