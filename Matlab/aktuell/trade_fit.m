function [tw_west, tw_east] = trade_fit()
%%% trade winds for fitting parameter theta 
% Laden der gemessenen Winddaten-Daten und Anpassen auf den Referenzzeitraum
% Jan 1980 bis Oct 2008 (da für die Thermoklinenhöhe nur Daten in diesem
% Zeitraum vorhanden sind)

%% NorthEast tradewind acting on west pacific - Anomaly Datas -
NE_tradewind=importdata('trade_west_anomaly.txt');
daten=NE_tradewind.data(2:30,2:end);   % fit fuer die Jahre 1980 - 2008
[N,~]=size(daten);
tw_west=daten(1,:);

for i=2:N
    tw_west=[tw_west, daten(i,:)];
end

tw_west=tw_west(1:346);    % Jan 1980 - Oct 2008 (346 Monate)

%% SouthEast tradewind acting on east pacific - Anomaly Datas -
SE_tradewind=importdata('trade_east_anomaly.txt');
daten=SE_tradewind.data(2:30,2:end);
[N,~]=size(daten);

tw_east=daten(1,:);

for i=2:N
    tw_east=[tw_east, daten(i,:)];
end

tw_east=tw_east(1:346);    % Jan 1980 - Oct 2008 (346 Monate)

end