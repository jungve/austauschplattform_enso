%Bestimmung des Parameters tetha der den gemessenen Gesamtwind in
%Hoehenanomalie der Thermokline verrechnet. Bestimmung zweier Parameter:
%West und Ost
%% Westen
% Datenladen
[tw_w, tw_e]=trade_fit();     % Gesamt gemessene Winddaten (Anomalien) in [850mbar]
[h_w, h_e]=thermocline_fit(); % Thermoklinen-Anomalien in [m]
SST=SST_fit();                % Seasurface Temperature im Osten (Anomalie) [K]


% Datenanpassung: 
tw_w=smoothdata(tw_w,'gaussian',10);  % Glaettung
h_w=smoothdata(h_w,'gaussian',10);

startdate = datenum('01-1980','mm-yyyy');
enddate = datenum('10-2008','mm-yyyy');
time = linspace(startdate,enddate,346);
% um thematik verschiebung zu zeigen
figure(100)
t=1:length(tw_w);
[ax, h1, h2] = plotyy(time,tw_w,time,h_w);
   set(h1,'LineWidth',1.5)
    set(h2,'LineWidth',1.5)
    set(get(ax(1),'Ylabel'),'String','trade winds [850mbar]','Fontsize',10)
    set(get(ax(2),'Ylabel'),'String','thermocline height [m]','Fontsize',10)
datetick('x','yyyy','keepticks')
title('Reaction of thermocline to wind anomaly west','Fontsize',13)
xlabel('time [1 month]','Fontsize',10)


%%% Bewertung zur Verschiebung
% "Wie viele Monate braucht die Thermokline, um auf dem Wind zu reagieren"
% h_w = tw_w + v
v=-5:1:15;

[theta,mw,std]=rating_theta(h_w, tw_w,v);

% am besten passendes Theta: => kleinster Mittlerer Fehler 
[min_w, ind_w]=min(mw);
theta_w=theta(ind_w);
v_w=v(ind_w);

%%%%% Visualiesierung
%%%% %%% Plot fuer gegenueberstellung SST-Anteil und h_gemessen %%%
% alpha=0.67;
% b=14;
% tau=alpha*b*(-SST);
% figure(21)
% plot(time, h_w,  time, tau,'LineWidth',1.5);
% refline(0,0);
% datetick('x','yyyy','keepticks')
% xlabel('Zeit t in [month]','Fontsize',12);
% ylabel('Anomalien in [m]','Fontsize',12);
% legend('h_w gemessen','SST Anteil','Fontsize',10);
% title('Gegenueberstellung h gemessen und SST-Anteil - Westen','Fontsize',13);




% %%% Visualisierung der Ergebnise im Westen
% % Fehlermittelwert in Abhängigkeit von der Verschiebung
% figure(1)
% hax=axes;
% hold on
% plot(v,mw,v,std,'LineWidth',1.5)
% line([6 6],get(hax,'YLim'),'Color','r','LineWidth',1.5)
% set(gca, 'XTickMode', 'manual', 'XTick', v);
% grid;
% legend('Mittelwert', 'Standartabweichung','Fontsize',10)
% title('Fehler bei Berechunug Thermoklinenanomalie aus theta (Westen)','Fontsize',13)
% xlabel('Verschiebung (Wind=h_W+v) [month]','Fontsize',12)
% ylabel('Fehler [m]','Fontsize',12)
% hold off
% % 
% Vergleich zwischen der mit theta berechneteten Thermoklinenhöhe und der gemessenen. 
figure (31)
hw_func=theta_w*tw_w;
plot(time,h_w, time,hw_func,'LineWidth',1.5)
datetick('x','yyyy','keepticks')
legend('h_w measured', 'h_w calculated out of wind data','Fontsize',10)
title('Thermokline west','Fontsize',13)
xlabel('time t in [month]','Fontsize',12)
ylabel('thermocline anomaly in [m]','Fontsize',12)
% 
% Händische verbesserung von Theta, damit die Peak-Höhe besser
% übereinstimmt
% figure (11)
% he_func=1.3*theta_w*tw_w;
% time=1:length(h_w);
% plot(time,h_w, time,he_func)
% legend('orginal gemessen', 'Annäherung')
% title('Thermokline Westen mit zusaetzlichem Faktor 1.3')

%% Osten
tw_e=smoothdata(tw_e,'gaussian',10);  % Glaettung
h_e=smoothdata(h_e,'gaussian',10);


 
% % um thematik verschiebung zu zeigen
% figure(200)
% t=1:length(tw_e);
% [ax, h1, h2] = plotyy(time,tw_e,time,h_e);
%    set(h1,'LineWidth',1.5)
%     set(h2,'LineWidth',1.5)
%     set(get(ax(1),'Ylabel'),'String','TradeWinds [850mbar]','FontSize',10)
%     set(get(ax(2),'Ylabel'),'String','Thermoklinehoehe [m]','FontSize',10)
% datetick('x','yyyy','keepticks')    
% title('Reaktionszeit Thermokline auf Windanomalie Osten','Fontsize',13)
% xlabel('time [1 month]','Fontsize',10)



v=-5:1:15;

[thetae,mwe,std]=rating_theta(h_e, tw_e,v);

% am besten passendes Theta:
[min_e, ind_e]=min(mwe);
theta_e=thetae(ind_e);
v_e=v(ind_e);
%%%% %%% Plot fuer gegenueberstellung SST-Anteil und h_gemessen %%%
%eps2=0.5;
gamma=0.077;
tau=(SST)/gamma;

figure(32)
plot(time, h_e,  time, tau,'LineWidth',1.5);
refline(0,0);
datetick('x','yyyy','keepticks')
xlabel('Zeit t in [month]','Fontsize',12);
ylabel('Anomalien in [m]','Fontsize',12);
legend('h_e gemessen','SST Anteil','Fontsize',10);
title('Gegenueberstellung h gemessen und SST-Anteil (gamma) - Osten','Fontsize',13);



% %%% Visualisierung der Ergebnisse im Osten
% % Fehlermittelwert in Abhängigkeit von der Verschiebung
% figure(2)
% hax=axes;
% hold on
% plot(v,mw,v,mwe,'LineWidth',1.5)
% plot([-4 -4],[mwe(2),0],[6 6],[mw(12),0],'LineWidth',1.5)
% % line([6 6],get(hax,'YLim'),'LineWidth',1.5)
% % line([-4 -4],get(hax,'YLim'),'LineWidth',1.5)
% set(gca, 'XTickMode', 'manual', 'XTick', v);
% ylim([0 14])
% grid;
% legend('Mittelwert Westen', 'Mittelwert Osten','Fontsize',10)
% title('Fehler bei Berechunug Thermoklinenanomalie aus theta','Fontsize',13)
% xlabel('Verschiebung v (Wind=h+v) [month]','Fontsize',12)
% ylabel('Fehler [m]','Fontsize',12)
% hold off

% Händische verbesserung von Theta, damit die Peak-Höhe besser
% übereinstimmt
figure (12)
he_func=1.3*theta_e*tw_e;
time=1:length(h_e);
plot(time,h_e, time,he_func)
legend('orginal gemessen', 'Annäherung')
title('Thermokline Osten mit zusaetzlichem Faktor 1.4')

% Vergleich zwischen der mit theta berechneteten Thermoklinenhöhe und der gemessenen. 
% he_func= -9 * tw_e;
figure (22)
he_func=theta_e*tw_e;

plot(time,h_e, time,he_func,'LineWidth',1.5)
datetick('x','yyyy','keepticks')
legend('h_e gemessen', 'h_e aus Gesamt-Wind berechnet','Fontsize',10)
title('Thermokline Osten','Fontsize',13)
xlabel('Zeit t in [month]','Fontsize',12)
ylabel('Thermoklinenanomalie in [m]','Fontsize',12)

