function [curve, fouRei] = externtradewindWest()
%EXTERNTRADEWIND 
% Funktion extrahiert den netto-Passatwindanteil aus dem gemessenen Wind
% ueber dem Westpazifik. Hierzu wird der gemessene Gesamt-Wind in eine
% Hoehenanomalie umgerechnet (mittels des bestimmten Parameters tetha_west) und
% mit dem Anteil aus SST verrechnet.
% --> externer Systeminput: netto Passatwinde

%% ggf theta und verschiebung von extern
% [theta_w, theta_e, v_w, v_e] = parameter_theta();

theta = 5.0592;

%% NorthEast tradewind acting on west pacific - Anomaly Datas -

NE_tradewind=importdata('trade_west_anomaly.txt');
daten=NE_tradewind.data(:,2:end);   % verfuegbar fuer die Jahre 1979 - 2019
[N,~]=size(daten);
tw_west=daten(1,:);

for i=2:N
    tw_west=[tw_west, daten(i,:)];
end                         

tw_west=smoothdata(tw_west,'gaussian',10);  % Glaettung


%% Bestimmung des einflusses des passatwindes ohne SST Anteil
SST=importdata('SST.txt');
SST_E=SST.data(350:841,6);  % Jan 1979 - 2019, passend zu verfuegbaren Winddaten; 6.Spalte fuer Ost Anomalie
SST_E=smoothdata(SST_E,'gaussian',10);

b=14;
alpha=0.67;
r=1/6.25;
tau=alpha*b*(-SST_E);          % tau: Einflussanteil an der Thermoklinenhoehenaenderung erzeugt durch die SST


hw_func=theta*tw_west;        % VZ bereits beachtet

netto_tradewind=(hw_func-tau');     

time=1:length(SST_E);

%%%%%%%%%%%%%% "Funktionen"fit aus den verrechneten und extrahierten
%%%%%%%%%%%%%% Messwerten

curve = fit(time',netto_tradewind','smoothingspline');
startdate = datenum('01-1979','mm-yyyy');
enddate = datenum('12-2019','mm-yyyy');
dt = linspace(startdate,enddate,492);

%%%%%%%%%%%%%% moegliche Prognose mittels Fourierreihe %%%%%%%%%%%%%%
fouRei = fit(time',netto_tradewind','fourier8');
coeffs = coeffvalues(fouRei);
coeffs(:,18)=0.01345;  %Entspricht der Grundfrequenz im Osten
fouRei = fit(time',netto_tradewind','fourier8', 'StartPoint', coeffs);
% 
% fou= @(t) fouRei(t)+f3(t);    %�berlagerung beide Fourierreihen

startdate = datenum('01-1979','mm-yyyy');
enddate = datenum('10-2044','mm-yyyy');
dt2 = linspace(startdate,enddate,800);
zeit=1:800;


% %%%%%%%%%%%%%% Visualisierung Windzusammensetzung %%%%%%%%%%%%%%
% figure(11)
% plot(dt, hw_func, dt, netto_tradewind, dt, tau,'LineWidth',1.5);
% refline(0,0);
% datetick('x','yyyy','keepticks')
% xlabel('time t in [month]','Fontsize',12);
% ylabel('anomaly in [m]','Fontsize',12);
% legend('h_w total, calculated','net trade wind','SST part','Fontsize',10);
% title('Wind profile west','Fontsize',13);




%%%%%%%%%%%%%% moegliche Prognose mittels Fourierreihe %%%%%%%%%%%%%%
% figure(198)
%  plot(dt,netto_tradewind, dt2 ,fouRei(zeit),'LineWidth',1.5)
%  datetick('x','yyyy','keepticks')
%  legend('orginal', 'FourierReihe','Fontsize',10)
%  title('Fourierreihe f�r Wind-Prognose West','Fontsize',13)
%  xlabel('Zeit t in [month]','Fontsize',12);
%  ylabel('Anomalien in [m]','Fontsize',12);
%  xlim([startdate,enddate]);
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
end

