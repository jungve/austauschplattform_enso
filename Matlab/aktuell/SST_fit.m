 function SST_E = SST_fit()
%%% Seasurface Temperature for fitting of parameter theta
% Laden der gemessenen SST-Daten und Anpassen auf den Referenzzeitraum
% Jan 1980 bis Oct 2008 (da für die Thermoklinenhöhe nur Daten in diesem
% Zeitraum vorhanden sind)

SST=importdata('SST.txt');
SST_E=SST.data(361:706,6);  % Jan 1980 - Oct 2008, 6.Spalte fuer Ost Anomalie
end