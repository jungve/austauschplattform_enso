function [soi] = SOI_fit()
%%% Southern-Oscillation-Index  
% Laden der gemessenen SOI-Daten als Bewertungsindex f�r unser System,
% Daten von Jan 1979 bis Dez 2019

%% Southern Oscilation Index
SOI_Data=importdata('SOI_Equatorial.txt');
daten=SOI_Data(31:end-1,2:end);   % Daten erst ab 1979 relevant
[N,~]=size(daten);
soi=daten(1,:);

for i=2:N
    soi=[soi, daten(i,:)];
end

end