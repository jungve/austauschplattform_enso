%%%%%%%%%%%%%%%% Parameter Setting %%%%%%%%%%%%%%%%
r=1/6.25;
alpha=0.67;
eps1=1/2.75;
eps2=1/2;
gamma=0.077;
b=14;
% test
%%%%%%%%%%%%%%%% Definition System %%%%%%%%%%%%%%%% (hhT == (h_W, h_E, T_E) in ([m], [m], [K])
% Externe Anretungen Westen und Osten
[curvewest, fourierWest]=externtradewindWest();
[curveeast, fourierEast]=externtradewindEast();

d_dt = @(t,hhT)   [ -r * hhT(1)                         - r* alpha*b*hhT(3)     - r * curvewest(t)      ;      % Anregung Nordpassat 
                    eps2*hhT(1) -   eps2*hhT(2)         + eps2*b*hhT(3)         - eps2 * curveeast(t)   ;      % Anregung Suedpassat VZ umgedreht, weil der passat in der Windzusammensetzung falsches vz hat
                    %1.2 ost 0.8 west
                                      eps1*gamma*hhT(2)   - eps1*hhT(3)                                   ];
                                                      
%%% Eigenwerte %%%     % ggf raunehmen... ggf im Bericht thematisieren fuer
%%% Eigenschwingung etc.
A= [    -r,         0,      -r*alpha*b ;    
        eps2,   - eps2,     eps2 * b; 
        0 ,     eps1*gamma,    - eps1];
lambda = eig(A);

%%%%%%%%%%%%%%%% Systemverhalten (Jan 79 - Dez 2019)  %%%%%%%%%%%%%%%%
time=1:1:492;      % [month]   

[t, hhT] = ode45(d_dt, time , [-2.14 5.82 0.38] );

% hTundTe(t,hhT(:,2),hhT(:,1),hhT(:,3));

%
%%%%%%%%%%%%%%%% Systemverhalten (Prognose)  %%%%%%%%%%%%%%%%
%%%%%%% 
% Differentialgleichungssystem
d_dt2 = @(t2,hhT2)   [ -r * hhT2(1)                         - r* alpha*b*hhT2(3)     - r * fourierWest(t2)      ;      % Anregung Nordpassat
                    eps2*hhT2(1) -   eps2*hhT2(2)         + eps2*b*hhT2(3)         - eps2 * fourierEast(t2)   ;      % Anregung Suedpassat
                                    eps1*gamma*hhT2(2)   - eps1*hhT2(3)                                   ];
time2=1:1:624;              % [month]  bis Dec '30

[t2, hhT2] = ode45(d_dt2, time2 , [-2.14 5.82 0.38] );

%%
%%%%%%%%%%%%%%%%  Vergleich mit ENSO index soi equatorial   %%%%%%%%%%%%%%%%
soi=SOI_fit();
soi=smoothdata(soi,'gaussian',10);

soi_h=soi_calc(hhT(:,1),hhT(:,2));
R = corrcoef(soi,soi_h);
R = R(2,1);                  
%Korrelations-Koeffizient: Wie stark stimmen die beiden Kurven �berein?
%Maximale �bereinstimmung f�r R=1;


%%
%%%%%%%%%%%%%%%% Orginaldaten Import zum Vergleich %%%%%%%%%%%%%%%%
[h_w , h_e] = thermocline_fit();
SST = importdata('SST.txt');
SST_E=SST.data(350:841,6);
% Daten-Gl�tten
h_w=smoothdata(h_w,'gaussian',10);
h_e=smoothdata(h_e,'gaussian',10);
SST_E=smoothdata(SST_E,'gaussian',10);
% %%
% %%%%%%%%%%%%%%%% Visualisierung %%%%%%%%%%%%%%%%
% Fuer Plot mit angezeigten Jahren
startdate = datenum('01-1979','mm-yyyy');
enddate = datenum('12-2019','mm-yyyy');
dt = linspace(startdate,enddate,492);
% 
%%% Simulationsergebnisse Modell
figure(1)
plot(dt, hhT(:,1), dt, hhT(:,2) , dt, hhT(:,3), 'LineWidth', 1.5)
grid;
datetick('x','yyyy','keepticks')
title('Reproduction ENSO - stimulated with trade winds 1979 - 2019', 'Fontsize', 13)
legend('h_W in [m]', 'h_E in [m]', 'temperature in [K]', 'Fontsize', 10);
xlabel('time t in [month]', 'Fontsize', 12)
ylabel('anomaly', 'Fontsize', 12)


%%% Vergleichsplots Simulation und Orginaldaten
t=dt(13:358);                  
figure(2)
plot(t, hhT(13:358,1), t, h_w, 'LineWidth', 1.5)
datetick('x','yyyy','keepticks')
grid;
title('h_W comparison: with extern trade winds 1980 - 2008', 'Fontsize', 13)
legend('h_W simulated', 'h_W measured', 'Fontsize', 10);
xlabel('time t in [month]', 'Fontsize', 12)
ylabel('height anomaly in [m]', 'Fontsize', 12)

figure(3)
plot(t, hhT(13:358,2), t,  h_e, 'LineWidth', 1.5)
datetick('x','yyyy','keepticks')
grid;
title('h_E comparison: with extern trade winds 1980 - 2008', 'Fontsize', 13)
legend('h_E simulated','h_E measured', 'Fontsize', 10);
xlabel('time t in [month]', 'Fontsize', 12)
ylabel('height anomaly in [m]', 'Fontsize', 12)

figure(4)
plot(dt, hhT(:,3), dt,  SST_E, 'LineWidth', 1.5)
datetick('x','yyyy','keepticks')
grid;
title('SST comparison: with extern trade winds 1980 - 2008', 'Fontsize', 13)
legend('temperature simulated','temperature measured', 'Fontsize', 10);
xlabel('time t in [month]', 'Fontsize', 12)
ylabel('temperature anomaly in [K]', 'Fontsize', 12)


%%% Vergleich SOI
figure(100)
plot(dt,-soi, dt, soi_h,dt, hhT(:,3), 'LineWidth', 1.5)
refline(0,0)
grid;
datetick('x','yyyy','keepticks')
title('Comparison with EQSOI 1979 - 2019', 'Fontsize', 13)
legend('negative EQSOI','SOI calculated with thermocline depth','temperature in [K]', 'Fontsize', 10, 'Location','Southwest');
xlabel('time t in [month]', 'Fontsize', 12)
ylabel('index/ anomaly', 'Fontsize', 12)

%%%%%%%%%%%%%% Prognose %%%%%%%%%%
% Fuer Plot mit angezeigten Jahren
startdate = datenum('01-1979','mm-yyyy');
enddate = datenum('12-2030','mm-yyyy');
dt2 = linspace(startdate,enddate,624);

figure(5)
plot(dt2, hhT2(:,1), dt2, hhT2(:,2) , dt2, hhT2(:,3), 'LineWidth', 1.5)
grid;
datetick('x','yyyy','keepticks')
title('Forecast until Dec. 2030 using Fourier series', 'Fontsize', 13)
legend('h_W in [m]', 'h_E in [m]', 'temperature in [K]', 'Fontsize', 10);
xlabel('time t in [month]', 'Fontsize', 12)
ylabel('anomaly', 'Fontsize', 12)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% nur fuer Plot ohne Anregung....
d_dt = @(t,hhT)   [ -r * hhT(1)                         - r* alpha*b*hhT(3)           ;     
                    eps2*hhT(1) -   eps2*hhT(2)         + eps2*b*hhT(3)            ;     
                   
                                      eps1*gamma*hhT(2)   - eps1*hhT(3)                                   ];
 time=1:1:250;
                                  [t, hhT] = ode45(d_dt, time , [-30 35 5] );
 
 figure(10)
plot(t, hhT(:,1), t, hhT(:,2) , t, hhT(:,3), 'LineWidth', 1.5)
grid;
title('Recharge Oscillator', 'Fontsize', 13)
legend('h_W in [m]', 'h_E in [m]', 'temperature in [K]', 'Fontsize', 10);
xlabel('time t in [month]', 'Fontsize', 12)
ylabel('anomaly', 'Fontsize', 12)                                 