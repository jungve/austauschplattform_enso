function soi = soi_calc(h_w,h_e)

% %Standartisierung Westen
mh_w=mean(h_w);
sh_w=std(h_w);
stand_w=(h_w-mh_w)./sh_w;

%Standartisierung Osten
mh_e=mean(h_e);
sh_e=std(h_e);
stand_e=(h_e-mh_e)./sh_e;

msd=std(stand_e-stand_w);
soi=(stand_e-stand_w)/msd;

% Pdiff=h_e-h_w;
% msd=std(Pdiff);     %Monatliche Standartabweichung
% Pdiffav=mean(Pdiff);
% Pdiffav=Pdiffav.*ones(length(Pdiff),1);
% soi=-(Pdiff-Pdiffav)./msd;