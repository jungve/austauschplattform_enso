clear

a11O=-0.0488;
a12=0.021;
a21O=-1.322;
a22=-0.008;

c_tA=5.63*10^(-12);
mc=1025*3994*80;
c_tO=1.07*10^(-13);
r_tt=7.32*10^10;
r_tf=1.51*10^20;
%tau=@(t) sin(t);
f=0.0001;


d_dt = @(t,Th)   [a11O*Th(1) + a12*Th(2) + c_tA* rand  + 1/mc  * rand;  
    a21O*Th(1) + a22*Th(2)  + c_tO* r_tt * rand];


[t, Th] = ode45(d_dt, [0;  200] , [-0.3 0.3]   );

plot(t, Th(:,1), t, Th(:,2))
title('Frauen')
legend('Temperatur', 'Thermokline');
xlabel('Zeit t')
ylabel('Amplitude')
