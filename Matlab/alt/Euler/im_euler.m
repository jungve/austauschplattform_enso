function [t,y]=im_euler(f,t0,y0,tmax,N,f_,tol)
  h=(tmax-t0)/N;
  t=zeros(N+1,1);
  y=zeros(N+1,1);
  t(1)=t0;
  y(1)=y0;
  for i=2:1:N+1
    %g=diffial(f(t0+i,y(i)),y(i-1));
    %g=y(i-1)+h*f(t0+i,g);
    %g=fsolve(g,t0+i);
    %y(i)=g(2);
    t(i)=t0+h*(i-1);
    fun=@(g) y(i-1)+h*f(t(i),g)-g;
    y(i) = fsolve(fun,y(i-1));
    %y = [y yNext];
  end
end
