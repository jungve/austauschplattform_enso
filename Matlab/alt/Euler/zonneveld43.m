function [t,y] = zonneveld43(f,t0,y0,tmax,epsilon) 
p=4;
pd=3;
A=[0 0 0 0 0;
   0.5 0 0 0 0;
   0 0.5 0 0 0;
   0 0 1 0 0
   5/32 7/32 13/32 -1/32 0];
c=[0;0.5;0.5;1;3/4];
b=[1/6 1/3 1/3 1/6 0];
bd=[-0.5 7/3 7/3 13/6 -16/3];
s=length(b);

hp=0.1;
n=tmax-t0;
h=hp;

k=zeros(1,s);
t=t0:hp:tmax;
y=zeros(1,n);
y(1)=y0;
for u=2:n
for i=1:s
    ak=0;
    for j=1:i-1
        ak=ak+A(i,j)*k(j);
    end
    k(i)=f(t(u-1)+hp*c(i),hp*ak);
  while 1
    yd=y(u-1)+hp*bd(i)*k(i);
    y=y(u-1)+hp*b(i)*k(i);
    d=(yd-y)*hp^(p-pd);
    hp=(epsilon*hp^(p+1)/d)^1/p;
    h=[h,hp];
    if d<=epsilon*hp
      break;
    end
  end
  y(u)=y;
end
end

end
