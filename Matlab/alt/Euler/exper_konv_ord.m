function p= exper_konv_ord(methode,f,t0,y0,tmax,f_,tol,n0,m)
  y=zeros(1,m);
  p=zeros(1,m);
    for i=1:1:m
    [t,v]=methode(f,t0,y0,tmax,n0*2^(i-1),f_,tol);%liefert zeit-wertvektor
    y(i)=v(length(t));
    
    if i>2
       p(i)=log(norm(y(i-2)-y(i-1))/norm(y(i-1)-y(i)))/log(2); 
    end
    end

  
end
