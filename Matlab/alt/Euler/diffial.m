function  residual =diffial(y,t,MyFunc,h)
 residual=y+feval(MyFunc,t,y)*h-y;

endfunction