%Klassisches Runge-Kutta-Verfahren
A=[0 0 0 0;
   0.5 0 0 0;
   0 0.5 0 0;
  0 0 1 0];
c=[0;0.5;0.5;1];
b=[1/6 1/3 1/3 1/6];
%[t,y]=runge_kutta(A,b,c,@func,0,0,2,10,4,4)
p=stabilitaetsfunktion(A,b)

% %1/8-Verfahren
% A=[0 0 0 0;
%    1/3 0 0 0;
%    1/3 1 0 0;
%    1 -1 1 0];
% c=[0;1/3;2/3;1];
% b=[1/8 3/8 3/8 1/8];
% [t,y]=runge_kutta(A,b,c,@func,0,0,2,10,4,4)

% %Runge
% A=[0 0 0 0;
%    0.5 0 0 0;
%    0 1 0 0;
%    0 0 1 0];
% c=[0;0.5;1;1];
% b=[1/6 2/3 0 1/6];
% [t,y]=runge_kutta(A,b,c,@func,0,0,2,10,4,4)

%%Aufgabe 43
%A=[0 0 0;
%   1/10 0 0;
%   0 0.5 0];
%c=[0;1/10;0.5];
%b=[0 0 1];
%[t,y]=runge_kutta(A,b,c,@func,0,0,1,60,4,4)
