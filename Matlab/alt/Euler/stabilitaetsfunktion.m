function p= stabilitaetsfunktion(A,b)
n=length(b);
k=zeros(n,n);
for i=1:n
    k(i,1)=1;
    for j=1:i-1
        k(i,j)=A(i,j)*k(j,i-1);
    end 
end
p=1;
for i=1:n
    a=0;
    for j=1:n
        a=a+b(j)*k(i,j);
    end
    p=[p,a];
end
end 