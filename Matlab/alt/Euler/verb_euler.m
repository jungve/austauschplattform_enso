function [t,y]=verb_euler(f,t0,y0,tmax,N,f_,tol)
  h=(tmax-t0)/N;
  t=zeros(N+1,1);
  y=zeros(N+1,1);
  t(1)=t0;
  y(1)=y0;
  for i=2:1:N+1
    y(i)=y(i-1)+h*f(t(i-1)+h/2,y(i-1)+h/2*f(t(i-1),y(i-1)));
    t(i)=t0+h*(i-1);
  end
  %plot(t,y)
end