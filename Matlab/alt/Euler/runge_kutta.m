function [t,y]=runge_kutta(A,b,c,f,t0,y0,tmax,n,f_,tol)
h=(tmax-t0)/n;
s=length(b);
k=zeros(1,s);
t=t0:h:tmax;
y=zeros(1,n);
y(1)=y0;
for u=2:n
for i=1:s
    ak=0;
    for j=1:i-1
        ak=ak+A(i,j)*k(j);
    end
    k(i)=f(t(u-1)+h*c(i),h*ak);
    y(u)=y(u-1)+h*b(i)*k(i);
end
end
end