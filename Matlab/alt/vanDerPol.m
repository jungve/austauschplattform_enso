eps=2;

d_dt= @ (t,x) [x(2) ;
             eps *( 1- x(1)^2 ) * x(2) - x(1)];
        
 [t, vdP] = ode45(d_dt, [0;  1000] , [4 0 ]   ); 
 
 figure(1)
plot(t, vdP(:,1), t, vdP(:,2))
title('Van der Pol Oszillator')
legend('x1', 'x2' );
xlabel('Zeit t')
ylabel('Amplitude')



figure(2)
plot(vdP(:,1), vdP(:,2));
title('Van der Pol Oszillator')
legend('Trajektoren' );
xlabel('x1')
ylabel('x2')
