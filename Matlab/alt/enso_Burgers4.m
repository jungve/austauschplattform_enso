

a11=-0.08;
a12=0.17;
a21=-0.17;
a22=0.01;

zeta1= 0.25;
zeta2= 2.22;


d_dt = @(t,Th)   [a11*Th(1) + a12*Th(2) + zeta1;    a21*Th(1) + a22*Th(2)+ zeta2];


[t, Th] = ode45(d_dt, [0;  200] , [0 -2]   );

plot(t, Th(:,1), t, Th(:,2))

