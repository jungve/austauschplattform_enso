%time=1:length(h_w);

load('NorthEast_tw.mat')
load('h_w_anomalie.mat')



% Datenanpassung: 
NO_tw=NO_tw_A(13:end);                  % Winddaten verfuegbar von 1979-2019 aber Thermokline nur von 1980-2008
NO_tw=NO_tw(1:350);

NO_tw=smoothdata(NO_tw,'gaussian',20);  % Glaettung
h_w_a=smoothdata(h_w_a,'gaussian',20);

% Verschiebung v
v=7;
h=h_w_a(v+1:end);                            % Verschiebung (tradewinds vor n_w auf Grund von Reaktionszeit)
tw=NO_tw(1:end-v);
figure(100)
plot(h)
hold on 
plot(tw)
hold off

% Bestimmung eines Linearen Zusammenhangs zwischen Wind und Thermokline
fc = 'a(1) * wind';   
f  = inline(fc,'a','wind');
as = 5;
% Bestimmung Umrechnungsfaktor h_w_ges[m]= theta[m/mbar]*tradewinds[mbar]
theta = nlinfit(tw,h,f,as);


% figure (7)
% hn=theta*tw; 
% plot(t,h,t,hn)

% Untersuchung Guete des Parameters
hw_func=theta*NO_tw;

err = immse(hw_func(v+1:end),h);
maxf=max(abs(hw_func(v+1)-h));


%% Bestimmung des einflusses des passatwindes ohne SST Anteil
load('SST_E.mat')
time=1:length(h_w_a);
b=14;
alpha=0.67;
r=1/6.25;
tau=alpha*r*b*SST_E;    % tau: Einflussanteil an der Thermoklinenhoehenaenderung erzeugt durch die SST


netto_tradewind=hw_func+tau';    %% JETZT: AUD DIESEN DATEN DEN DATENFIT FUER UNSERE EXTERNE ANREGUNG MACHEN??


[curve, goodness, output] = fit(time',netto_tradewind','smoothingspline');
test=curve(time);
figure(20)
plot(time,test);
xlabel('Month');
ylabel('Pressure');

%% Visualisierung


figure(1)
plot(time, h_w_a);
  [ax, h1, h2] = plotyy(time,h_w_a,time,NO_tw);
   set(h1,'LineWidth',1)
    set(h2,'LineWidth',1)
   % set(ax(1),'FontWeight','Bold','LineWidth',1)
    %set(ax(2),'FontWeight','Bold','LineWidth',1)
    set(get(ax(1),'Ylabel'),'String','Thermoklinehoehe [m]','FontWeight','Bold')
    set(get(ax(2),'Ylabel'),'String','TradeWinds [850mbar]','FontWeight','Bold')
    
title('Thermocline West Pacific Jan 1980 - Oct 2008')
xlabel('time [1 month]')
ylabel('Geglaettete Verlaefe ohne Verschiebung Thermocline depth [m]')

  figure(2)
    plot(time(v+1:end), hw_func(v+1:end),  time(v+1:end), h);
    legend('hw_gesamt berechnet', 'hw_gesamt gemessen')
    title('Verschobene verlaeufe Thermocline West Pacific Jan 1980 - Oct 2008')

figure(10)
    plot(time, hw_func, time, tau, time, netto_tradewind);
    legend('hw_gesamt', 'hw_SST', 'hw_{trade}_{Netto}')
    

    
      