function Burgers3Anja()
%%% Winddaten einlesen
global NO_Wind;
global SO_Wind;
global tc;
[NO_Wind,SO_Wind,ende]=NEWindDaten();
tc=1;           %Zeitzugriff

r0 = [-8  2 10];

% Numerische Loesung im Zeitbereich t=0..tmax bestimmen
options = odeset('MaxStep', 0.01);
t = linspace(0, ende , 1000);
[t, u] = ode45(@ode_rhs, t, r0);
    
% Ortsvektor aus der Lösung extrahieren
r = u(:, 1:3);

% Bahnkurve darstellen
figure(1);
%plot(t, r(:,2), t, h);
plot(t,u);
legend('hw','Te', 'he');
grid;
xlabel('t');
ylabel('Anormalie');
title('Anregung mit NO-SO Passat')
    
end


function udot = ode_rhs(t, u)
 hT = u;
 global NO_Wind;
 global SO_Wind;
 global tc;
 
 
   r1 =1/6.25;
   epsilon2 = 1/2; %4.8;
   epsilon1 = 1/2.75;  
            % Anpassung Te an he (Upwelling)
            % 0.077 = Verzögerung 1 Mo
   gammah = 0.077; %0.1205;%0.077;
            % Anpassung hw an Wind
            % 0.67 = Verzögerung von 4 Mo
   alpha = 0.67; %0.6;%0.67; 

   b = 14;
   tau = b* hT(2);              % Wind Walkerzirkulation
  tauwest = NO_Wind(tc);
  tauost = SO_Wind(tc);
  if tc<=t+1
    tc=tc+1;
  end
 
 if isnan(tauwest)
     tauwest=0;
 end
  if isnan(tauost)
     tauost=0;
 end
  
   v(1) = - r1 * (hT(1) + alpha * tau + tauwest);      %Hw
   v(2) = - epsilon1 * (hT(2) - gammah * hT(3)-1/b*tauost);   %Te
   v(3) = - epsilon2 * (hT(3) - hT(1) -tau-  tauost);   %He

   udot = transpose(v);
end







