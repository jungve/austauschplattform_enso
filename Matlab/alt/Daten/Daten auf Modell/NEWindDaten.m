function [intNO_tw, intSO_tw,ende] = NEWindDaten()
%%%  Interpolation tradewinds acting on west pacific - standardized Datas -
% NorthEast tradewind
NE_tw=importdata('WestPacific_anomaly.txt');
save('WestPacific_anomaly','NE_tw');
datenNE=NE_tw.data(:,2:end);

% SouthEast tradewind
SE_tw=importdata('EastPacific_anomaly.txt');
save('EastPacific_anomaly','SE_tw');
datenSE=SE_tw.data(:,2:end);

% Thermokline Westen


[N,~]=size(datenSE);
NO_tw_A=datenNE(1,:);
SO_tw_A=datenSE(1,:);

for i=2:N
    NO_tw_A=[NO_tw_A, datenNE(i,:)];
    SO_tw_A=[SO_tw_A, datenSE(i,:)];
end
ende=length(NO_tw_A);
time=1:ende;

%%% Funktionsinterpolation 
% Fake Tradewind fuer Fit-Startparameter: f2=@(t) 0.1*(sin(0.527*t) + 2 * sin(0.1*t)+3);

tneu= linspace(0, ende , 1000);

intNO_tw=interp1(time, NO_tw_A, tneu);
intSO_tw=interp1(time, SO_tw_A, tneu);

% 
% figure(2)
% plot(tneu,vq)
% title('Interpolation')
% % legend('NO tradewind mbar');
% xlabel('time [1 month]')
% ylabel('NO tradewind [850 mbar]')

