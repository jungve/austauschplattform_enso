%%% SST-Daten
SST=importdata('SST_ElNino3_ElNino4_ElNino3.4.txt');
save('SST_ElNino3_ElNino4_ElNino3.4', 'SST');
datenNino3=SST.data(349:end-3,6);
datenNino4=SST.data(349:end-3,8);
[M,~]=size(datenNino3);

%%% Trade-wind Daten -NO-Passat_standart-
tradewind=importdata('WestPacific_standardData.txt');
save('WestPacific_standardData', 'tradewind');
datenNO_sD=tradewind.data(:,2:end);
[N,~]=size(datenNO_sD);
NO_tw_sD=datenNO_sD(1,:);

for i=2:N
    NO_tw_sD=[NO_tw_sD, datenNO_sD(i,:)];
end

NO_extAnr = NO_tw_sD - 0.1 .* transpose(datenNino4);    %wg. 1Pa=0.01mbar  
time=1:length(NO_extAnr);

%%% Fake Tradewind fuer Fit-Startparameter: f2=@(t) 0.1*(sin(0.527*t) + 2 * sin(0.1*t)+3);

fc = '(a(1) * sin( a(2)*t + a(3) )   +  a(4) * sin( a(5)*t +  a(6)))*a(7)';   % Vielleicht brauchen wir noch einen Freiheitsgrad fuer den Wachstum, damit der Fit nicht so smooth ist sondern auch die Versaerkung der Winde abbildet
f  = inline(fc,'a','t');
as = [3,0.527,0,3,0.1,0,3];
 
af = nlinfit(time,NO_extAnr,f,as)

y=f(af,time);

figure(1)
plot(time, NO_extAnr, time, NO_tw_sD, time, datenNino4);
title('NO Tradewinds 1979-2019 -stand. Data- externe Anregung (mit Fit)')
%legend('NO tradewind mbar');
xlabel('time [1 month]')
ylabel('NO tradewind [850 mbar]')