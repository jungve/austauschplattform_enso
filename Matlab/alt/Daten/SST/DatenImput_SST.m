%%% NorthEast tradewind acting on west pacific - standardized Datas -
SST=importdata('SST.txt');
save('SST.mat', 'SST');
SST_E=SST.data(362:end,6);  % Start 1980
SST_E=SST_E(1:350);         
save('SST_E.mat', 'SST_E')
b=14;
alpha=0.67;
r=1/6.25;
tau=alpha*r*b*SST_E;    % tau: Einflussanteil an der Thermoklinenhoehenaenderung erzeugt durch die SST


time=1:length(SST_E);

figure(15)
plot(time, SST_E, time, tau);
title('SST East Anomaly')
legend('SST [K]', 'dhw [m]');
xlabel('time [1 month]')
ylabel('SST Anomaly [K]')

