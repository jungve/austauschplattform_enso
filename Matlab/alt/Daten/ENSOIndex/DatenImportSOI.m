SOI=importdata('SOI_Equatorial.txt');
save('SOI_Equatorial.mat','SOI');
SOI=SOI(32:60,2:end);  % Start 1980
       


[N,~]=size(SOI);
soi=SOI(1,:);

for i=2:N
   soi=[soi, SOI(i,:)];
end
save('soi.mat','soi')

startdate = datenum('01-1980','mm-yyyy');
enddate = datenum('12-2008','mm-yyyy');
dt = linspace(startdate,enddate,348);

figure(11)
time=1:length(soi);
plot(dt,soi)
datetick('x','yyyy','keepticks')