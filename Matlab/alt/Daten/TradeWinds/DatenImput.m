%%% NorthEast tradewind acting on west pacific - standardized Datas -
NE_tw=importdata('WestPacific_standardData.txt');
% save('WestPacific_standardData', 'tradewind');
daten=NE_tw.data(:,2:end);
[N,~]=size(daten);
NO_tw_sD=daten(1,:);


for i=2:N
    NO_tw_sD=[NO_tw_sD, daten(i,:)];
end



time=1:length(NO_tw_sD);

figure(1)
plot(time, NO_tw_sD);
title('NO Tradewinds 1979-2019 - Stand. Data')
%legend('NO tradewind mbar');
xlabel('time [1 month]')
ylabel('NO tradewind [850 mbar]')

%%% Funktionen Fit 
% Fake Tradewind fuer Fit-Startparameter: f2=@(t) 0.1*(sin(0.527*t) + 2 * sin(0.1*t)+3);

fc = 'a(1) * sin( a(2)*t + a(3) )   +  a(4) * sin( a(5)*t +  a(6))';   % Vielleicht brauchen wir noch einen Freiheitsgrad fuer den Wachstum, damit der Fit nicht so smooth ist sondern auch die Versaerkung der Winde abbildet
f  = inline(fc,'a','t');
as = [0.1,0.5,0,2,0.1,3];
 
af = nlinfit(time,NO_tw_sD,f,as);

y=f(af,time);

figure(5)
plot(time,y)



%% NorthEast tradewind acting on west pacific - Anomaly Datas -
NE_tradewind=importdata('WestPacific_anomaly.txt');
save('NE_tradewind_anomaly', 'NE_tradewind');
daten=NE_tradewind.data(:,2:end);
[N,~]=size(daten);
NO_tw_A=daten(1,:);

for i=2:N
    NO_tw_A=[NO_tw_A, daten(i,:)];
end

time=1:length(NO_tw_A);

save('NorthEast_tw.mat','NO_tw_A')

figure(2)
plot(time, NO_tw_A);
title('NO Tradewinds 1979-2019 - Anomaly Data')
%legend('NO tradewind mbar');
xlabel('time [1 month]')
ylabel('NO tradewind [850 mbar]')

%% SouthEast tradewind acting on east pacific - standardized Datas -
tradewind=importdata('EastPacific_standardData.txt');
save('EastPacific_standardData', 'tradewind');
daten=tradewind.data(:,2:end);
[N,~]=size(daten);
SO_tw_sD=daten(1,:);

for i=2:N
    SO_tw_sD=[SO_tw_sD, daten(i,:)];
end

time=1:length(SO_tw_sD);

figure(3)
plot(time, SO_tw_sD);
title('SO Tradewinds 1979-2019 - stand. Data')
%legend('SO tradewind mbar');
xlabel('time [1 month]')
ylabel('SO tradewind [850 mbar]')

%% SouthEast tradewind acting on east pacific - Anomaly Datas -
SE_tradewind=importdata('EastPacific_anomaly.txt');
save('SE_tradewind_anomaly', 'SE_tradewind');
daten=SE_tradewind.data(:,2:end);
[N,~]=size(daten);
SO_tw_A=daten(1,:);

for i=2:N
    SO_tw_A=[SO_tw_A, daten(i,:)];
end

time=1:length(SO_tw_A);
save('SouthEast_tw.mat','SO_tw_A')

figure(4)
plot(time, SO_tw_A);
title('SO Tradewinds 1979-2019 - Anomaly Data')
%legend('SO tradewind mbar');
xlabel('time [1 month]')
ylabel('SO tradewind [850 mbar]')