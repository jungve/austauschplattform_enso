%%%  Interpolation NorthEast tradewind acting on west pacific - standardized Datas -
NE_tw=importdata('WestPacific_standardData.txt');
save('WestPacific_standardData', 'tradewind');
daten=tradewind.data(:,2:end);
[N,~]=size(daten);
NO_tw_sD=daten(1,:);

for i=2:N
    NO_tw_sD=[NO_tw_sD, daten(i,:)];
end

time=1:length(NO_tw_sD);

figure(1)
plot(time, NO_tw_sD);
title('Orginal Daten')
%legend('NO tradewind mbar');
xlabel('time [1 month]')
ylabel('NO tradewind [850 mbar]')

%%% Funktionsinterpolation 
% Fake Tradewind fuer Fit-Startparameter: f2=@(t) 0.1*(sin(0.527*t) + 2 * sin(0.1*t)+3);

tneu= linspace(0, length(NO_tw_sD) , 1000);

vq=interp1(time, NO_tw_sD, tneu);

figure(2)
plot(tneu,vq)
title('Interpolation')
%legend('NO tradewind mbar');
xlabel('time [1 month]')
ylabel('NO tradewind [850 mbar]')

