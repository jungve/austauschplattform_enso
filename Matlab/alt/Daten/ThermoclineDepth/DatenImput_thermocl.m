%%%  west pacific - gemessene Thermokline -
thermocline=importdata('WestPacific_hw.txt');
save('WestPacific_hw', 'thermocline');
[N,~]=size(thermocline);
h_w=thermocline(1,:);

for i=2:N
    h_w=[h_w, thermocline(i,:)];
end

save('h_w_gemessen.mat', 'h_w')

%% east pacific - gemessene Thermokline Tiefe -
thermocline=importdata('EastPacific_he.txt');
save('EastPacific_he', 'thermocline');

[N,~]=size(thermocline);
h_e=thermocline(1,:);

for i=2:N
    h_e=[h_e, thermocline(i,:)];
end

time=1:length(h_e);
save('h_e_gemessen.mat', 'h_e')

%%%  west pacific - anomaly Thermokline - 
thermocline=importdata('WestPacific_hw_anomalie.txt');
save('WestPacific_hw_anomalie', 'thermocline');
[N,~]=size(thermocline);
h_w_a=thermocline(1,:);

for i=2:N
    h_w_a=[h_w_a, thermocline(i,:)];
end

save('h_w_anomalie.mat', 'h_w_a')

%%%  west pacific - anomaly Thermokline - 
thermocline=importdata('EastPacific_he_anomalie.txt');
save('EastPacific_he_anomalie', 'thermocline');
[N,~]=size(thermocline);
h_e_a=thermocline(1,:);

for i=2:N
    h_e_a=[h_e_a, thermocline(i,:)];
end

save('h_e_anomalie.mat', 'h_e_a')



load('SE_tradewind_anomaly.mat')
SE_tw=SE_tradewind.data(:,2:end);
[N,~]=size(SE_tw);
SO_tw_A=SE_tw(1,:);


for i=2:N
    SO_tw_A=[SO_tw_A, SE_tw(i,:)];
end

SO_tw_A=SO_tw_A(13:end);
SO_tw_A=SO_tw_A(1:350);


SO_tw_A=smoothdata(-SO_tw_A);
h_e=smoothdata(h_e);




%SST-Daten zum Vergleich
load('SST.mat')
datenNino3=SST.data(361:710,5);
datenNino4=SST.data(361:710,8);


figure(2)
% plot(time, h_e, time , SO_tw_A);
%  [ax, h1, h2] = plotyy(time,h_e,time, SO_tw_A);
plot(time, h_e, time , datenNino3);
  [ax, h1, h2] = plotyy(time,h_e,time, datenNino3);
   set(h1,'LineWidth',0.5)
    set(h2,'LineWidth',0.5)
   % set(ax(1),'FontWeight','Bold','LineWidth',1)
    %set(ax(2),'FontWeight','Bold','LineWidth',1)
    set(get(ax(1),'Ylabel'),'String','Thermoklinehoehe [m]','FontWeight','Bold')
%     set(get(ax(2),'Ylabel'),'String','TradeWinds [850mbar]','FontWeight','Bold')
    set(get(ax(2),'Ylabel'),'String','SST [�C]','FontWeight','Bold')
title('Thermocline East Pacific Jan 1980 - Oct 2008')
xlabel('time [1 month]')
ylabel('Thermocline depth [m]')
