%% JIN %%
c=1;            %[-]
gamma=0.75;     %[-]
r=0.25;         %[-]
alpha=0.125;    %[-]
b_0=2.5; %[-]
mue=0.60;   %mue element aus [0 , 1.5]

b=b_0*mue;      %[-]
R=gamma*b-c;    %[-]

d_dt = @(t,hT)   [-r*hT(1)  - alpha*b*hT(2) ;    gamma*hT(1) + R*hT(2)];

lambda1= eig([-r , -alpha*b; gamma, R]);

[t, hT] = ode45(d_dt, [0;  120] , [-0.19 0.384]  ); % Andre AW wg [h]=150m, [T]=7,5K

hT(:,1)=hT(:,1)*150;
hT(:,2)=hT(:,2)*7.5;
figure(1)
p = plot(2*t, hT(:,1), 2*t, hT(:,2),'LineWidth',1.5)
p(2).Color=[0.9290 0.6940 0.1250];
title('JIN-Modell - 1997','Fontsize',13)
legend('Thermokline h_W','Temperatur T_E','Fontsize',10);
xlabel('time [month]','Fontsize',12)
ylabel('Temperature T_E [K], depth h_W [m]','Fontsize',12)
grid

%% Burgers (1)%% 
clear; 
%%%%%%%% aus JIN hergeleitet %%%%%%%%%
mue=0.15; 
r= 1/8; % nach Jin in [1/month]
alpha=(1/16); % in [1/month] nach JIN
eps1=1/2;        %eps1=c in [1/month] (nach Jin)
b=50*mue; % [m/�C] nach Jin
gamma=1.5/(2*10); % [�C/(month *m] nach Jin

gammah=gamma/(eps1);
alpha=alpha/r;

r=1/6.25;
alpha=0.67;
eps1=1/2.75;
%epsilon2=1/2;
gammah=0.077;
b=14;


% time=1:1:240;
d_dt = @(t,hT)   [-r*(hT(1)  + alpha*b*hT(2) );   eps1* gammah*hT(1) + eps1*(-1 +gammah*b)*hT(2)];

lambda2= eig([-r , -r*alpha*b; eps1*gammah, eps1*(-1+gammah*b)]);

[t, hT] = ode45(d_dt, [0 240] , [-28.5 2.88]   );

% %Plot Mit Jahreszahlen
% startdate = datenum('12-1982','mm-yyyy');
% enddate = datenum('11-2002','mm-yyyy');
% dt = linspace(startdate,enddate, 240);

figure(2)
p=plot(t, hT(:,1), t, hT(:,2),'LineWidth', 1.5)
p(2).Color=[0.9290 0.6940 0.1250];
title('Burgers-Modell (2b) - Parameterwerte von Burgers','Fontsize',13)
legend('Thermokline h_W','Temperatur T_E','Fontsize',10);
xlabel('time [month]','Fontsize',12)
ylabel('Temperature T_E [K], depth h_W [m]','Fontsize',12)
% set(gca,'ytick',([-30:10:30]))
axis([0 250 -30 30])
grid

%% Burgers (3) 
clear;
r=1/6.25;
alpha=0.67;
epsilon1=1/2.75;
epsilon2=1/2;
gamma=0.077;
b=14;



d_dt = @(t,hhT)   [ -r * hhT(1)                       - r* alpha*b*hhT(3); % - sin(0.1*t);
                    epsilon2*hhT(1) - epsilon2*hhT(2) + epsilon2*b*hhT(3);%+ sin(0.1*t) ; 
                                epsilon1*gamma*hhT(2) - epsilon1*hhT(3) ];


A= [    -r,         0,              -r*alpha*b ;    
        epsilon2,   - epsilon2,     epsilon2 * b; 
        0 ,         epsilon1*gamma, - epsilon1];
lambda3 = eig(A);

[t, hhT] = ode45(d_dt, [0;  240] , [-28.5 35.7 2.88]   );

figure(3)
plot(t, hhT(:,1), t, hhT(:,2) , t, hhT(:,3), 'LineWidth', 1.5)
% title('Burgers-Modell (3)','Fontsize',13)
legend('h_W', 'h_E', 'temperature T_E','Fontsize',10 );
xlabel('time t [month]','Fontsize',12)
ylabel('anomaly h_W and h_E in [m], T_E in [K]','Fontsize',12)
grid

