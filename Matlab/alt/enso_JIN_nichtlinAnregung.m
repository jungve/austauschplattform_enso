c=1;
gamma=0.75;
r=0.25;
alpha=0.125;
b_0=2.5;
mue=0.7;   %mue element aus [0 , 1.5]

b=b_0*mue;
R=gamma*b-c;

en=1;



d_dt = @(t,hT)   [-r*hT(1)  - alpha*b*hT(2) ;    gamma*hT(1) + R*hT(2) - en*( hT(1) + b* hT(2))^3];

[t, hT] = ode45(d_dt, [0;  120] , [-1 1]   );

figure(1)
plot(t, hT(:,1), t, hT(:,2))
title('JIN-Modell (1) - 1997 - nichtlineare Anregung')
legend('Thermokline h_W','Temperatur');
xlabel('Zeit t')
ylabel('Amplitude')

figure(2)
plot(hT(:,1), hT(:,2))
title('JIN-Modell (1) - 1997 - nichtlineare Anregung')
xlabel('Thermokline h_W')
ylabel('Temperatur')