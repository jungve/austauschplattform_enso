startdate = datenum('01-1980','mm-yyyy');
enddate = datenum('12-2008','mm-yyyy');
dt = linspace(startdate,enddate,350);

figure(30)
plot(dt,hw_func)
datetick('x','yyyy','keepticks')