clear;
%%% Parameter Setting %%%
r=1/6.25;
alpha=0.67;
epsilon1=1/2.75;
epsilon2=1/2;
gamma=0.077;
b=14;

%%% System %%% (hhT == (h_W, h_E, T_E) in ([m], [m], [K])

d_dt = @(t,hhT)   [ -r * hhT(1)                          - r* alpha*b*hhT(3)    - 0.6*(2*cos(0.527*t)   + 2*(cos(0.1*t)));      % Anregung Nordpassat
                    epsilon2*hhT(1) - epsilon2*hhT(2)    + epsilon2*b*hhT(3)    + 0.2*(sin(0.527*t+6)   + 3*(sin(0.13*t)));     % Anregung Suedpassat
                                epsilon1*gamma*hhT(2)    - epsilon1*hhT(3)      ]    ;%+ 0.15*cos(0.017*t)];                    % Anregung IDO - auskommentiert
                           
%%% Eigenwerte %%%
A= [    -r,         0,              -r*alpha*b ;    
        epsilon2,   - epsilon2,     epsilon2 * b; 
        0 ,         epsilon1*gamma, - epsilon1];
lambda = eig(A);

%%% Systemverhalten %%%
time=0:0.1:600;      % [month]

[t, hhT] = ode45(d_dt, time , [-10 10 5] );

figure(1)
plot(t, hhT(:,1), t, hhT(:,2) , t, hhT(:,3))
title('Burgers-Model (3)')
legend('h_W', 'h_E', 'Temperatur' );
xlabel('Zeit t')
ylabel('Amplitude')

% figure(2)
% plot3(hhT(:,1), hhT(:,2), hhT(:,3));
% title('Burgers-Model (3)')
% legend('Trajektoren' );
% xlabel('h_W')
% ylabel('h_E')
% zlabel('Temperatur')
% 
% figure(3)
% plot(hhT(:,1), hhT(:,2));
% title('Burgers-Model (3)')
% legend('Trajektoren' );
% xlabel('h_W')
% ylabel('h_E')

% hTundTe(t,hhT(:,2),hhT(:,1),hhT(:,3));