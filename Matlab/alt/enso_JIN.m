c=1;
gamma=3;
r=0.25;
alpha=0.125;
b_0=2.5;
mue=0.166;   %mue element aus [0 , 1.5]

b=b_0*mue;
R=gamma*b-c;





d_dt = @(t,hT)   [-r*hT(1)  - alpha*b*hT(2) ;    gamma*hT(1) + R*hT(2)];

[t, hT] = ode45(d_dt, [0;  100] , [-1 1]   );

figure(1)
plot(t, hT(:,1), t, hT(:,2))
title('JIN-Modell (1) - 1997')
legend('Thermokline h_W','Temperatur');
xlabel('time [2 month]')
ylabel('Temperature [7.5 K], depth h_w [150 m]')
figure(2)
plot(hT(:,1), hT(:,2))