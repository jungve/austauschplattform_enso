function Burgers3Anja()
%%% Winddaten einlesen
[NW_Wind,ende]=NEWindDaten();

r0 = [10  -4 -15];
%v0 = [0  0];
u0 = [r0];

% Numerische Loesung im Zeitbereich t=0..tmax bestimmen
options = odeset('MaxStep', 0.01);
t = linspace(0, ende , 1000);
[t, u] = ode45(@ode_rhs, t, u0);
    
% Ortsvektor aus der Lösung extrahieren
r = u(:, 1:3);
%h=0.5*(r(:,1)+r(:,3));

% hTundTe(t,r(:,3),r(:,1),r(:,2))
% thermokline(t,r(:,3),r(:,1))

% Bahnkurve darstellen
figure(1);
%plot(t, r(:,2), t, h);
plot(t,u);
legend('hw','Te', 'he');
grid;
xlabel('t');
ylabel('Anormalie');
title('Anregung mit NO-SO Passat')
    
end


function udot = ode_rhs(t, u)
 hT = u;
 
   r1 =1/6.25;
   epsilon2 = 1/2; %4.8;
   epsilon1 = 1/2.75;
   
            % Anpassung Te an he (Upwelling)
            % 0.077 = Verzögerung 1 Mo
   gammah = 0.077; %0.1205;%0.077;

            % Anpassung hw an Wind
            % 0.67 = Verzögerung von 4 Mo
   alpha = 0.67; %0.6;%0.67; 

   b = 14;
   tau = b* hT(2);              % Wind Walkerzirkulation
%    tau2= 0.265*(cos(0.527*t)+ 2.5*(cos(0.1*t)) );% NO-Passat 
%    tau3 = 0.37*(cos(0.527*t)+ 1.6*(sin(0.138*t)) );  % SO-Passat
%    tau4 = 0.1*sin(0.02*t);          % IPO
  tautest = NW_Wind(t*1000);
%    tau=tau+tautest;

   
   v(1) = - r1 * (hT(1) + alpha * tau )- tautest; %r1* b * tau2;       %Hw
   v(2) = - epsilon1 * (hT(2) - gammah * hT(3)-tautest);%tau3);%-1/b*tautest);%+ tau4;    %Te
   v(3) = - epsilon2 * (hT(3) - hT(1) -tau - tautest);% b * tau3);   %He
    
   % a = 0;
   udot = transpose(v);
end







