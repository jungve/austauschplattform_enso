function hTundTe(t,he,hw,Te)
 l=-3:0.01:3;
 
 % An Durchschnittshöhe 20° Thermokline anpassen
 hw = hw - 200;
 he = he - 200;
 
 c = jet(32);       %Farbcode Temperatur

 
% Prepare the new file. 
vidObj=VideoWriter('Thermokline.avi');
open(vidObj);

for j = 1:length(t)
    % Aktualisiere Mittlere Thermokline
    h= 1/2 * (he(j)+hw(j));
    tk=ones([1,length(l)]).*(h);
    
    % Aktualisiere Thermokline
    ip=interp1([3 -3],[he(j) hw(j)],l, 'pchip');
    
    % Aktualisier Temperatur-Farbe
    idx = round(interp1([min(Te) max(Te)], [1 32], Te(j)));
    
    % Plot Thermokline
    plot(l,ip,'r',l,tk,'b')
   
    hold on

    % Plot Temperatur 
    plot(2,-140,'o',...
    'Markersize',40,...
    'MarkerFaceColor',c(idx,:))

    axis([-3 3 -230 -100])    
    grid;
    xlabel('Längengrade');
    ylabel('h');
    [~,icons,~,~] = legend('Thermokline','mittlere Thermokline', 'Temperatur');
    icons(9).MarkerSize=10;

    hold off
%     colorbar;
% Write each frame to the file.
currFrame = getframe();
writeVideo(vidObj,currFrame);
end

 % Close the file.
    close(vidObj);

 
  
% clear all
end


