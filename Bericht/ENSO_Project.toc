\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Einleitung}{4}{section.1}%
\contentsline {section}{\numberline {2}Themenhinführung}{4}{section.2}%
\contentsline {subsection}{\numberline {2.1}Das Phänomen El Ni\~{n}o und El Ni\~{n}o Southern Oscillation (ENSO)}{4}{subsection.2.1}%
\contentsline {subsubsection}{\numberline {2.1.1}Die Neutralphase}{6}{subsubsection.2.1.1}%
\contentsline {subsubsection}{\numberline {2.1.2}Die Warmphase (El Ni\~{n}o)}{7}{subsubsection.2.1.2}%
\contentsline {subsubsection}{\numberline {2.1.3}Die Kaltphase (La Ni\~{n}a)}{7}{subsubsection.2.1.3}%
\contentsline {subsection}{\numberline {2.2}Geophysikalische Begrifflichkeiten und Phänomene im Rahmen des El Ni\~{n}o}{8}{subsection.2.2}%
\contentsline {section}{\numberline {3}Mathematisches ENSO-Modell}{9}{section.3}%
\contentsline {subsection}{\numberline {3.1}Einführung}{9}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Recharge Oszillationsmodell}{9}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Modellvorstellung}{10}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}Modelldiskussion}{12}{subsubsection.3.2.2}%
\contentsline {section}{\numberline {4}Äußere Einflüsse}{14}{section.4}%
\contentsline {subsection}{\numberline {4.1}Passatwinde}{14}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Einbinden der Passatwinde als externe Systemanregung}{15}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Darstellung der resultierenden Windanteile über dem Ost- und Westpazifik}{19}{subsection.4.3}%
\contentsline {section}{\numberline {5}Simulationsergebnisse}{20}{section.5}%
\contentsline {subsection}{\numberline {5.1}Abbildung des historischen Verlaufs von 1979 bis 2019 mit dem Recharge Oscillator}{20}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Vergleich der simulierten Werte mit realen Messwerten}{21}{subsection.5.2}%
\contentsline {subsubsection}{\numberline {5.2.1}Vergleich der Thermoklinenhöhenanomalie}{22}{subsubsection.5.2.1}%
\contentsline {subsubsection}{\numberline {5.2.2}Vergleich der Oberflächenwasser-Temperaturanomalie}{22}{subsubsection.5.2.2}%
\contentsline {subsection}{\numberline {5.3}Vergleich mit dem Equatorial Southern Oscillation Index (EQSOI)}{23}{subsection.5.3}%
\contentsline {subsubsection}{\numberline {5.3.1}Überblick gängiger ENSO - Indizes}{23}{subsubsection.5.3.1}%
\contentsline {subsubsection}{\numberline {5.3.2}Definition eines Index anhand Thermoklinenhöhenanomalien T-SOI}{25}{subsubsection.5.3.2}%
\contentsline {subsubsection}{\numberline {5.3.3}Vergleich des T-SOI mit dem EQSOI}{26}{subsubsection.5.3.3}%
\contentsline {subsection}{\numberline {5.4}Prognose}{27}{subsection.5.4}%
\contentsline {subsubsection}{\numberline {5.4.1}Erstellung einer Fourierreihe für die externe Systemanregung}{27}{subsubsection.5.4.1}%
\contentsline {subsubsection}{\numberline {5.4.2}Einbindung der Fourierreihe als externe Systemanregung}{29}{subsubsection.5.4.2}%
\contentsline {section}{\numberline {6}Diskussion und Ausblick}{30}{section.6}%
\contentsline {section}{\numberline {7}Literatur}{32}{section.7}%
