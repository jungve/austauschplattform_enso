\NeedsTeXFormat{LaTeX2e}\relax
\ProvidesClass{ScriptGerman}
\LoadClass[11pt]{article}

% Schrift, Sprache und Encoding
\RequirePackage{fourier}
\RequirePackage[ngerman]{babel}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}


% Allgemeine Geometrie
\RequirePackage{geometry}
\geometry{a4paper}
\geometry{margin=2cm, inner=2cm, outer=2cm}


% Absatzformatierung und Zeilenabstand
\usepackage[parfill]{parskip}
\linespread{1.3}


% Ueberschriften nicht im Blocksatz
\RequirePackage{sectsty}
\allsectionsfont{\raggedright}


% Falls URLs zitiert werden
\RequirePackage{url}


% Erm�glicht kurze mehrspaltige Abschnitte
\RequirePackage{multicol}


% Formelsatz
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{amsthm}


% Einheiten richtig setzen
\RequirePackage{siunitx} % c=\SI{3e8}{km/s} oder \si{kV}
\sisetup{exponent-product = \cdot}
\sisetup{output-decimal-marker = {,}}


% Zeichen f�r die W�hrung Euro
\usepackage{eurosym} % \euro{}


% Imagin�re Einheit (damit kann man einfach auf j umstellen)
\newcommand{\ci}{\mathrm{i}}
\DeclareMathOperator{\re}{Re}
\DeclareMathOperator{\im}{Im}


% Abstand Interpuktionen in abgesetzten Gleichungen
\newcommand{\eqip}[1]{\ #1}


% Tabellen
\RequirePackage{booktabs}     % Sch�ne Tabellen
\RequirePackage{supertabular} % Tabellen �ber mehr als eine Seite


% Grafiken
\RequirePackage{graphicx}
\RequirePackage{color}

% Kopfzeilen und Seitennumerierung
\RequirePackage{totpages}
\setlength{\headheight}{15pt}
\AtBeginDocument{
  \RequirePackage{fancyhdr}
  \fancypagestyle{plain}{
    \fancyhf{}
    \fancyhead[L]{}
    \fancyhead[C]{}
    \fancyhead[R]{}
    \fancyfoot[L]{\slshape \MyAuthor}
    \fancyfoot[R]{\slshape \MyTitle}
    \renewcommand{\headrulewidth}{0pt}
    \renewcommand{\footrulewidth}{0.5pt}
  }
  \fancypagestyle{normal}{
    \fancyhf{}
    \fancyhead[L]{\slshape \leftmark}
    \fancyhead[C]{}
    \fancyhead[R]{\thepage}
    \fancyfoot[L]{\slshape \MyAuthor}
    \fancyfoot[R]{\slshape \MyTitle}
    \renewcommand{\headrulewidth}{0.5pt}
    \renewcommand{\footrulewidth}{0.5pt}
  }

  \pagestyle{normal}
  \renewcommand{\sectionmark}[1]{\markboth{\thesection.\ #1}{}}

  % Title und Author in andere Variable speichern, weil \maketitle diese
  % makros \@title und \@author l�scht.
  \makeatletter
  \let\MyTitle\@title
  \let\MyAuthor\@author
  \let\MyDate\@date
  \makeatother
}




% Haengende Fussnoten
\setlength{\footnotesep}{\baselinestretch\footnotesep}
\setlength{\footnotesep}{1.2\footnotesep}
\newlength{\fbreite}
\long\def\@makefntext#1{%
   \parindent 1em%
   \noindent
   \setlength{\fbreite}{\columnwidth}
   \addtolength{\fbreite}{-1.6em}
   \hbox to 1.2em{\hss\@makefnmark\ }\parbox[t]{\fbreite}{#1}}


% Inhaltsverzeichnis
   \renewcommand{\tableofcontents}{%
      \section*{\contentsname \@mkboth{\contentsname}{}}%
      \@starttoc{toc}}%
   % Abst�nde im Inhaltsverzeichnis anpasssen
   \makeatletter
   \renewcommand*{\l@section}{\@dottedtocline{2}{1.7em}{2.0em}}
   \renewcommand*{\l@subsection}{\@dottedtocline{2}{4.7em}{3.5em}}
   \makeatother



% Neudefinition von Trennregeln. 
% \moderate ist etwas zwischen zwischen \sloppy und \fuzzy
\newcommand{\moderate}
{%
  \tolerance 1414
  \hbadness 1414
  \emergencystretch 1.5em
  \hfuzz 0.3pt
  \vfuzz \hfuzz
  \relax
  \clubpenalty10000
  \widowpenalty10000
}
\AtBeginDocument{\moderate}




% Load the Hyperref-package if pdf is produced
\RequirePackage{ifpdf}
\ifpdf
   % Anklickbare Querverweise im PDF
   \RequirePackage[
     pdfborder={0 0 0}, 
     plainpages=false, 
     pdfpagelabels]{hyperref}
\else
   % Diesen Befehl als Dummy definieren, falls man kein PDF erzeugt,
   % damit das tex-File trotzdem korrekt durchl�uft.
   \newcommand{\texorpdfstring}[2]{#1}
\fi


% thebibliogrpahy aus der article-Klasse kopiert und das *
% bei section entfernt, damit die �berschrift nummeriert wird.
\makeatletter
\renewenvironment{thebibliography}[1]
     {\section{\refname}%
      %\@mkboth{\MakeUppercase\refname}{\MakeUppercase\refname}%
      \list{\@biblabel{\@arabic\c@enumiv}}%
           {\settowidth\labelwidth{\@biblabel{#1}}%
            \leftmargin\labelwidth
            \advance\leftmargin\labelsep
            \@openbib@code
            \usecounter{enumiv}%
            \let\p@enumiv\@empty
            \renewcommand\theenumiv{\@arabic\c@enumiv}}%
      \sloppy
      \clubpenalty4000
      \@clubpenalty \clubpenalty
      \widowpenalty4000%
      \sfcode`\.\@m}
     {\def\@noitemerr
       {\@latex@warning{Empty `thebibliography' environment}}%
      \endlist}
\makeatother

%% Im Titel, das Wort Projektbericht hinzuf�gen
\makeatletter
\def\@maketitle{%
  \newpage
  \null
  \vskip 2em%
  \begin{center}%
  \let \footnote \thanks
    {\LARGE Anwendungsprojekt \\ \@title \par}%
    \vskip 1.5em%
    {\large
      \lineskip .5em%
      \begin{tabular}[t]{c}%
        \@author
      \end{tabular}\par}%
    \vskip 1em%
    {\large \@date}%
  \end{center}%
  \par
  \vskip 1.5em}
\makeatother

%% Algorithmus - Paket siehe ftp://ftp.fu-berlin.de/tex/CTAN/macros/latex/contrib/algorithm2e/doc/algorithm2e.pdf
\RequirePackage[ngerman]{algorithm2e}

\endinput


